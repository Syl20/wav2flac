#!/usr/bin/env python3
# -*-coding:Utf-8 -*


"""
    log.py
    ======

"""

import logging

def set_log(logFile,options):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    formatter_file=logging.Formatter('%(asctime)s [%(levelname)s] %(message)s.',datefmt='%Y %b %d %H:%M:%S %Z')
    formatter_stdout=logging.Formatter('[%(levelname)s] %(message)s.')

    log_to_file=logging.FileHandler(logFile)
    log_to_file.setLevel(logging.DEBUG)
    log_to_file.setFormatter(formatter_file)
    logger.addHandler(log_to_file)

    log_to_screen=logging.StreamHandler()
    if options.verbose:
        log_to_screen.setLevel(logging.INFO)
    else:
        log_to_screen.setLevel(logging.ERROR)
    log_to_screen.setFormatter(formatter_stdout)
    logger.addHandler(log_to_screen)

    if options.debug:
        log_to_screen=logging.StreamHandler()
        log_to_screen.setLevel(logging.DEBUG)
        log_to_screen.setFormatter(formatter_stdout)
        logger.addHandler(log_to_screen)

    return logger
