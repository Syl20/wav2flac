#!/bin/env python3
# -*-coding:Utf-8 -*

"""
    configuration.py
    ================

"""

import optparse
import os

logFile="test.log"
defaultFolder = "./"
codecs=["wav","flac","mp3"]

def check_path(option, opt_str, value, parser):
    value = os.path.abspath(value)
    if os.path.isdir(value):
        setattr(parser.values, option.dest, value)
    else:
        parser.error("{} is not a directory".format(value))


def options_parser():

    usage = '%prog [options] {}'.format(codecs)
    description = 'Compress picture and send to remote dav location.'
    example = 'some examples'
    opt = optparse.OptionParser(usage=usage,description=description,epilog=example)
    opt.add_option('-f','--folder',action='callback',callback=check_path,type=str,default=defaultFolder,
                    help='Folder where are wav files to compress. Default is {}.'.format(str(defaultFolder)))
    opt.add_option('-v','--verbose',action='store_true',
                    help='Verbose mode.')
    opt.add_option('-d','--debug',action='store_true',
                    help='Debug mode.')
    opt.add_option('--progress', action='store_true',
                    help='Show progress')

    (options,args) = opt.parse_args()
    for arg in args:
        if arg not in codecs:
            opt.error("{} is not a valid codec".format(arg))
    return (options,args)
