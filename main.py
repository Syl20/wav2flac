#!/bin/env python3
# -*-coding:Utf-8 -*

"""
    main.py
    =======
"""

import configuration
from log import *
from functions import *
import os
from error import *

def main():

    (options,args) = configuration.options_parser()
    logger=set_log(configuration.logFile,options)

    logging.info("__ Start __")

    for option,value in options.__dict__.items():
        logging.info("Options {} is set to {}".format(option,value))
    logging.info("Codecs are {}".format(str(args)))

    # Scan working folder
    tracks_list = get_tracks_names(options.folder)
    for file in os.scandir(options.folder):
        if file.is_file() and extension(file) in ".wav" and file.name.split(' ')[0] in "Track":
            logging.info("{} found".format(file.name))
            logging.debug(file.path)
            logging.debug(directory(file))
            for codec in args:
                logging.debug(new_file(file,tracks_list,codec))
                compress(file,new_file(file,tracks_list,codec),codec)







if __name__ == '__main__':
    try:
        main()
        exit(0)
    except KnownError as error:
        logging.critical(error)
        exit(error.errorLevel)
    except Exception:
        logging.exception("Unknown Error :")
        logging.critical(
            "Merci de remonter l'erreur avec la trace à "
            "https://gitlab.com/mon/projet/issues.",
        )
        exit(1)
