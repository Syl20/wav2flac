#!/usr/bin/env python3
# -*-coding:Utf-8 -*

"""
    wavto.py
    ========

    Compress and rename wav files from CD to flac/mp3

    wavto.py <directory> <codecs>

    :Example:
    $ cd Album

"""


from subprocess import Popen
import sys
import os
from log import *

def get_tracks_names(folder):
    tracks = folder + "/tracks.txt"
    f = open(tracks,'r')
    tracks  = f.readlines()
    f.close()
    return tracks

def extension(file):
    """Return extension of file"""
    return os.path.splitext(file)[1]

def directory(path):
    """Return full path parent directory of path"""
    return os.path.abspath(os.path.dirname(path))

def track_number(file_name):
    """Return track number"""
    return file_name.split(os.extsep)[-2].split(' ')[1]

def new_file(file,tracks_list,codec):
    """Return new file path"""
    number = track_number(file.name)
    new_file_name = "{} - {}.{}".format(number,tracks_list[int(number) - 1].rstrip(),codec)
    new_directory = directory(file)
    return os.path.join(new_directory,new_file_name)

def compress(file,new_file_path,codec):
    stdout = open("/dev/null",'w')
    stderr = open(new_file_path + ".log",'w')
    logging.info("Compress to {}".format(new_file_path))

    if codec == "mp3":
        bitrate = " -b:a 320k "
    else:
        bitrate = " "

    process = Popen('ffmpeg -y -i ' + '"' + file.path + '" ' + bitrate + '"' + new_file_path + '"', shell = True, stdout = stdout, stderr = stderr)
    return_code = process.wait()
    stdout.close()
    stderr.close()
    return return_code


if __name__ == '__main__':
    for files in os.scandir("test"):
        print(directory(os.path.abspath(files.path)))
        print(new_track_name(files.name,["test","testé","te"]))
